\chapter{Explicit Form of Drilling Displacement}
By assuming the element is anti-clockwise encoded by four nodes labelled from \num{1} to \num{4}, the global coordinates of which are
\begin{gather}
\begin{bmatrix}
	x_1 & x_2 & x_3 & x_4 \\
	y_1 & y_2 & y_3 & y_4
\end{bmatrix}^\mT.
\end{gather}
For convenience, the following symbols are used as well.
\begin{gather}
\begin{array}{llll}
	x_{21}=x_2-x_1, & x_{32}=x_3-x_2, & x_{43}=x_4-x_3, & x_{14}=x_1-x_4, \\
	y_{21}=y_2-y_1, & y_{32}=y_3-y_2, & y_{43}=y_4-y_3, & y_{14}=y_1-y_4.
\end{array}
\end{gather}

For a given integration point with parent coordinates $\xi$ and $\eta$, denote
\begin{gather}
\xi_p=\xi+1,\quad
\xi_m=\xi-1,\quad
\eta_p=\eta+1,\quad
\eta_m=\eta-1,
\end{gather}
so that in \eqsref{eq:interpolation_conforming_total},
\begin{gather}
\mathbold{\phi}_d\mathbold{G}^{-1}\mathbold{Q}=\dfrac{1}{16}
\begin{bmatrix}
	x_{21}(1-\xi^2)\eta_m+x_{14}\xi_m(\eta^2-1) & y_{21}(1-\xi^2)\eta_m+y_{14}\xi_m(\eta^2-1) \\
	x_{21}(\xi^2-1)\eta_m+x_{32}\xi_p(\eta^2-1) & y_{21}(\xi^2-1)\eta_m+y_{32}\xi_p(\eta^2-1) \\
	x_{43}(\xi^2-1)\eta_p+x_{32}\xi_p(1-\eta^2) & y_{43}(\xi^2-1)\eta_p+y_{32}\xi_p(1-\eta^2) \\
	x_{43}(1-\xi^2)\eta_p+x_{14}\xi_m(1-\eta^2) & y_{43}(1-\xi^2)\eta_p+y_{14}\xi_m(1-\eta^2)
\end{bmatrix}^\mT.
\end{gather}
The derivatives can be computed accordingly.
\EoC
\chapter{Explicit Eigenvalue Decomposition of a Square GCMQ Element}
A unit square element with unit thickness is analysed with a density of \num{100} and an elastic modulus of \num{100}. The Poisson's ratio is set to \num{0.2}. GCMQG element is used as the reference element. The stiffness $\mathbold{K}$ can be explicitly expressed with discrete numbers.
\begin{scriptsize}
\begin{gather*}
\setlength{\arraycolsep}{4pt}
K=\left[\begin{array}{rrrrrrrrrrrr}
	 46.38 &  15.63 & -4.46 & -25.55 &  -5.21 &  4.46 & -26.54 & -15.63 & -2.48 &   5.70 &   5.21 &  2.48 \\
	 15.63 &  46.38 &  4.46 &   5.21 &   5.70 & -2.48 & -15.63 & -26.54 &  2.48 &  -5.21 & -25.55 & -4.46 \\
	 -4.46 &   4.46 &  2.68 &   4.46 &   2.48 & -1.44 &   2.48 &  -2.48 &  0.20 &  -2.48 &  -4.46 & -1.44 \\
	-25.55 &   5.21 &  4.46 &  46.38 & -15.63 & -4.46 &   5.70 &  -5.21 &  2.48 & -26.54 &  15.63 & -2.48 \\
	 -5.21 &   5.70 &  2.48 & -15.63 &  46.38 & -4.46 &   5.21 & -25.55 &  4.46 &  15.63 & -26.54 & -2.48 \\
	  4.46 &  -2.48 & -1.44 &  -4.46 &  -4.46 &  2.68 &  -2.48 &   4.46 & -1.44 &   2.48 &   2.48 &  0.20 \\
	-26.54 & -15.63 &  2.48 &   5.70 &   5.21 & -2.48 &  46.38 &  15.63 &  4.46 & -25.55 &  -5.21 & -4.46 \\
	-15.63 & -26.54 & -2.48 &  -5.21 & -25.55 &  4.46 &  15.63 &  46.38 & -4.46 &   5.21 &   5.70 &  2.48 \\
	 -2.48 &   2.48 &  0.20 &   2.48 &   4.46 & -1.44 &   4.46 &  -4.46 &  2.68 &  -4.46 &  -2.48 & -1.44 \\
	  5.70 &  -5.21 & -2.48 & -26.54 &  15.63 &  2.48 & -25.55 &   5.21 & -4.46 &  46.38 & -15.63 &  4.46 \\
	  5.21 & -25.55 & -4.46 &  15.63 & -26.54 &  2.48 &  -5.21 &   5.70 & -2.48 & -15.63 &  46.38 &  4.46 \\
	  2.48 &  -4.46 & -1.44 &  -2.48 &  -2.48 &  0.20 &  -4.46 &   2.48 & -1.44 &   4.46 &   4.46 &  2.68
\end{array}\right].
\end{gather*}
\end{scriptsize}

Eigendecomposition of $\mathbold{K}$ gives the following eigenvalues.
\begin{gather*}
\lambda=\left[\begin{array}{rrrrrrrrrrrr}
	0.00 & 0.00 & 0.00 & 0.00 & 1.05 & 2.06 & 2.06 & 40.10 & 40.10 & 83.33 & 88.02 & 125.00
\end{array}\right].
\end{gather*}
Clearly, the rank of $\mathbold{K}$ equals \num{8}. There are four rigid body modes. Mode 7 and mode 8 have the same eigenvalue, as well as mode 9 and mode 10. In total, there are \num{6} different eigen modes. The corresponding eigenvectors are
\begin{scriptsize}
\begin{gather*}
\left[\begin{array}{rrrrrrrr}
	    1 &     1 &       1 &     1 &     1 &  1 &     1 &  1 \\
	   -1 &     0 &  -13.34 & -2.74 &     0 &  1 &    -1 &  1 \\
	 5.92 &  9.48 &  135.99 & -0.39 & -0.11 &  0 & -0.34 &  0 \\
	   -1 &    -1 &      -1 &    -1 &    -1 &  1 &    -1 & -1 \\
	   -1 &     0 &   13.34 &  2.74 &     0 & -1 &    -1 &  1 \\
	-5.92 & -9.48 &  117.03 & -0.18 &  0.11 &  0 &  0.34 &  0 \\
	   -1 &     1 &       1 &     1 &     1 & -1 &    -1 & -1 \\
	    1 &     0 &  -13.34 & -2.74 &     0 & -1 &     1 & -1 \\
	 5.92 & -9.48 & -135.99 &  0.39 &  0.11 &  0 & -0.34 &  0 \\
	    1 &    -1 &      -1 &    -1 &    -1 & -1 &     1 &  1 \\
	    1 &     0 &   13.34 &  2.74 &     0 &  1 &     1 & -1 \\
	-5.92 &  9.48 & -117.03 &  0.18 & -0.11 &  0 &  0.34 &  0
\end{array}\right].
\end{gather*}
\end{scriptsize}

The corresponding mass matrix $\mathbold{M}$ can be seen as
\begin{scriptsize}
\begin{gather*}
\setlength{\arraycolsep}{4pt}
M=\left[\begin{array}{rrrrrrrrrrrr}
	11.11 &       &       &  5.56 &       &       &  2.78 &       &       &  5.56 &       &       \\
	      & 11.11 &       &       &  5.56 &       &       &  2.78 &       &       &  5.56 &       \\
	      &       &  0.56 &       &       & -0.14 &       &       & -0.28 &       &       & -0.14 \\
	 5.56 &       &       & 11.11 &       &       &  5.56 &       &       &  2.78 &       &       \\
	      &  5.56 &       &       & 11.11 &       &       &  5.56 &       &       &  2.78 &       \\
	      &       & -0.14 &       &       &  0.56 &       &       & -0.14 &       &       & -0.28 \\
	 2.78 &       &       &  5.56 &       &       & 11.11 &       &       &  5.56 &       &       \\
	      &  2.78 &       &       &  5.56 &       &       & 11.11 &       &       &  5.56 &       \\
	      &       & -0.28 &       &       & -0.14 &       &       &  0.56 &       &       & -0.14 \\
	 5.56 &       &       &  2.78 &       &       &  5.56 &       &       & 11.11 &       &       \\
	      &  5.56 &       &       &  2.78 &       &       &  5.56 &       &       & 11.11 &       \\
	      &       & -0.14 &       &       & -0.28 &       &       & -0.14 &       &       &  0.56
\end{array}\right].
\end{gather*}
\end{scriptsize}
The generalised eigenvalue problem can also be solved. This could be useful when it comes to the formulation of elemental damping.
\EoC