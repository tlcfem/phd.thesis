\chapter{Material Models}\label{material}
This chapter presents all existing material models used in this work and serves as a reference. Simple models such as the von Mises model are first introduced. They are used to investigate the performance baseline of (S)GCMQ regarding elasto-plastic applications. Since reinforced concrete shear wall specimens are also involved in this work, models of reinforcement, plain concrete and reinforced concrete are discussed, respectively. Noting that a number of material models are discussed, each model is independent to the others so that the same symbol may have different meanings in different models.
\section{The von Mises Model}
The von Mises model is probably the simplest plastic model and can be used to model metals. The yield surface of which can be characterised by the following function,
\begin{gather}
F=\lvert\mathbold{s}-\mathbold{\beta}\rvert-\sqrt{\dfrac{2}{3}}\sigma_y,
\end{gather}
in which $\mathbold{s}$ is the deviatoric stress, $\mathbold{\beta}(\mathbold\varepsilon_p)$ is called the back stress that can be defined as a function of plastic strain $\mathbold\varepsilon_p$ and/or other internal history variables, as is the yield stress $\sigma_y(\mathbold\varepsilon_p)$. An associative plastic flow is normally assumed for metal so that
\begin{gather}
\mathbold{\dot\varepsilon}_p=\gamma\pfrac{F}{\mathbold\sigma},
\end{gather}
in which $\gamma$ is the plastic consistency parameter. For hardening, a multilinear hardening relationship and an exponential form with a saturation limit are often used for isotropic hardening $\sigma_y(\mathbold\varepsilon_p)$. For kinematic hardening $\mathbold{\beta}(\mathbold\varepsilon_p)$, either linear or non-linear forms can be defined. Details can be seen elsewhere \citep[see, e.g.,][]{Simo1998,Chaboche2008}.
%\section{The Drucker--Prager Model}
%The Drucker--Prager Model is another simple material model that can be used to model soil behaviour. The corresponding Drucker--Prager yield surface is defined as
%\begin{gather}
%F=\sqrt{J_2}+\eta{}p-\xi{}c,
%\end{gather}
%where $J_2=\dfrac{1}{2}\mathbold{s}:\mathbold{s}$ is the second invariant of the deviatoric stress $\mathbold{s}$, $c(\bar{\varepsilon}_p)$ is the cohesion that can be defined as a function of equivalent plastic strain $\bar{\varepsilon}_p$, $\eta$ and $\xi$ are two constants that control the shape of yield surface, $p=\dfrac{1}{3}I_1$ is the hydrostatic pressure.
%
%The flow potential is defined as
%\begin{gather}
%G=\sqrt{J_2}+\bar{\eta}p,
%\end{gather}
%where $\bar{\eta}$ is another constant that controls dilatancy. If $\bar{\eta}=\eta$, then an associative flow rule is recovered.
%
%Different hardening rules can be implemented via defining cohesion $c(\bar{\varepsilon}_p)$. $\bar\eta$, $\eta$ and $\xi$ are normally defined as functions of frictional angle $\phi$. The yield surface can be calibrated to fit other yield surfaces, such as the Mohr--Columb model and the Tresca model, under various conditions. A detailed discussion, along with numerical implementation of the model, can be found elsewhere \citep{SouzaNeto2008}.
\section{Reinforcement}
With a discrete simulation approach, reinforcing bars are often idealized as uniaxial elements such as trusses which adopt uniaxial material models.

The uniaxial implementation of the aforementioned von Mises model can be used to model reinforcing bars. The Bauschinger effect can be modelled by incorporating a non-linear kinematic hardening rule, for example the Armstrong-Frederick rule \citep{Frederick2007}. Other popular uniaxial models include the models based on the Ramberg-Osgood relationship \citep{Ramberg1943}, the Menegotto-Pinto model \citep{Menegotto1973} and the Dodd-Restrepo model \citep{Dodd1995}.

The strain-stress relationship of the Menegotto-Pinto model can be expressed as
\begin{gather}
\sigma_n=b\varepsilon_n+\dfrac{\varepsilon_n-b\varepsilon_n}{\sqrt[R]{1+\varepsilon_n^R}},
\end{gather}
with the normalized stress $\sigma_n$ and strain $\varepsilon_n$ and parameter $R$ that controls curvature defined as
\begin{gather}
\sigma_n=\dfrac{\sigma-\sigma_r}{\sigma_0-\sigma_r},\quad\varepsilon_n=\dfrac{\varepsilon-\varepsilon_r}{\varepsilon_0-\varepsilon_r},\quad{}R=R_0-\dfrac{a_1\xi}{a_2+\xi},\quad\xi^n=\dfrac{\left|\varepsilon_r^{n}-\varepsilon_0^{n-1}\right|}{\varepsilon_{y,0}},\qquad(n>1).
\end{gather}
The other parameters are: $b$ controls hardening, $\sigma_y=\sigma_0^0$ and $\varepsilon_y=\varepsilon_0^0$ are initial yielding stress and strain so that $E=\sigma_y/\varepsilon_y$ defines Young's modulus, and three dimensionless parameters with recommended values $R_0=20$, $a_1=18.5$ and $a_2=0.15$. $\xi$ controls the Bauschinger effect. It can be set to zero so that $R=R_0$ remains unchanged for the whole loading history. As the result, the corresponding response resembles the one of a bilinear hardening material. Additional parameters may be introduced to account for isotropic hardening \citep[see][]{Filippou1983}. An illustrative model is shown in \figref{fig:mpf_model}.
\begin{figure}[htb]
\scriptsize\centering
\input{PIC/MAT/MPSM}
\caption{illustration of a typical Menegotto-Pinto model}\label{fig:mpf_model}
\end{figure}
Noting that most steel models exhibit the ratcheting mechanism which would result in the increment of the maximum strain without increasing the maximum stress under cyclic loading.

If reinforcement is implemented in a smeared approach, it is in general difficult to model bar buckling failure precisely. A popular solution is to directly modify the corresponding intact material model by, for example, adding a degradation factor. \citet{Dhakal2002} proposed a general modification that can be used with any existing models. It shall be stressed that bar buckling may play a vital role in modelling failure of slender walls. The degradation part of hysteresis loops may be largely affected and controlled by bar buckling. 
%\subsection{Steel}
%As a homogeneous material, steel can be well described by the classic plasticity theory. A simple bilinear or multilinear mixed hardening model with the von Mises yield criterion and the associated flow rule can be used. Similar to 1D models, the mixed hardening rule can be adopted to simulate the Bauschinger effect. Detailed discussion can be seen elsewhere \citep{Simo1998}. Here the multilinear backbone curve, which allows the simulation of the strength degradation caused by for example buckling and rupture, is briefly discussed.
%
%The multilinear backbone consists of several piece-wise linear segments. For convenience, it is often defined in terms of plastic strain $\mathbold{\varepsilon}_p$. It can be related to total strain $\mathbold{\varepsilon}$ by the additive decomposition,
%\begin{gather*}
%\mathbold{\varepsilon}_e=\mathbold{\varepsilon}-\mathbold{\varepsilon}_p=\mathbold{E}^{-1}\mathbold{\sigma},
%\end{gather*}
%where $\mathbold{\varepsilon}_e$ is the elastic strain. After the last input point, a constant response is assumed. For kinematic hardening, the centre of the yield surface is assumed to move towards the direction defined by the first and last point.
%\begin{figure}[htb]
%\centering\footnotesize
%\begin{subfigure}[b]{.48\textwidth}\centering
%\begin{tikzpicture}[x=1cm,y=1cm]
%\draw[->](-.3,0)--++(6,0)node[below=1mm]{$\varepsilon_p$ (\num{E-3})};
%\draw[->](0,-.3)--++(0,4)node[left=1mm]{$\sigma$};
%\draw[very thick,red](0,1.5)node[below right=2mm]{$P_1$}--(1,2.5)node[below=2mm]{$P_2$}--(2,3)node[below=2mm]{$P_3$}--(3,2.5)node[right=2mm]{$P_4$}--(4,1)node[below=2mm]{$P_5$}--(5,1);
%\end{tikzpicture}
%\caption{backbone in terms of $\varepsilon_p$}
%\end{subfigure}\quad
%\begin{subfigure}[b]{.48\textwidth}\centering
%\begin{tikzpicture}[x=1cm,y=1cm]
%\draw[->](-.3,0)--++(6,0)node[below=1mm]{$\varepsilon$ (\num{E-3})};
%\draw[->](0,-.3)--++(0,4)node[left=1mm]{$\sigma$};
%\draw[very thick,red](0,0)--(0+1.5/10,1.5)node[below right=2mm]{$P_1$}--(1+2.5/10,2.5)node[below=2mm]{$P_2$}--(2+3/10,3)node[below=2mm]{$P_3$}--(3+2.5/10,2.5)node[right=2mm]{$P_4$}--(4+1/10,1)node[below=2mm]{$P_5$}--(5+1/10,1);
%\end{tikzpicture}
%\caption{backbone in terms of $\varepsilon$}
%\end{subfigure}
%\caption{multilinear hardening material model}\label{fig:multilinear}
%\end{figure}
\section{Concrete}\label{sec:cdp}
The plastic-damage model proposed by \citet{Lee1998} is used to model concrete in-plane behaviour with the assist of a plane stress wrapper. A similar model known as the concrete damage plasticity (CDP) model, which supports user-defined backbones and degradations, is available in ABAQUS.

The uniaxial backbone curve is defined as a function of the accumulated plastic strain $\varepsilon_p$ by the following expression for both tension and compression,
\begin{gather}
\sigma_\aleph=f_\aleph\left(\left(1+a_\aleph\right)\exp\left(-b_\aleph\varepsilon_p\right)-a_\aleph\exp\left(-2b_\aleph\varepsilon_p\right)\right),
\end{gather}
where the subscript $\aleph$ can be either $t$ or $c$ to represent tensile and compressive properties, $f_\aleph$ is the initial yield strength, $a_\aleph$ and $b_\aleph$ are two parameters that control the shape of the curve. An illustration of backbones can be seen in \figref{fig:cdp_backbone}.
\begin{figure}[htb]
\centering\scriptsize
\begin{subfigure}{.48\textwidth}\centering
\begin{tikzpicture}
\begin{axis}[width=7.5cm,height=5.5cm,grid=major,domain=0:4,xmin=0,xmax=4,ymin=0,ymax=1.5,xlabel=plastic strain $\varepsilon_p$,ylabel=normalised $\sigma_c$,xticklabels={,,},yticklabels={,,}]
\addplot[samples=100,blue,very thick,fill=gray,fill opacity=0.5]{4*exp(-x)-3*exp(-2*x)}\closedcycle;
\end{axis}
\end{tikzpicture}
\caption{compression}
\end{subfigure}
\begin{subfigure}{.48\textwidth}\centering
\begin{tikzpicture}
\begin{axis}[width=7.5cm,height=5.5cm,grid=major,domain=0:4,xmin=0,xmax=4,ymin=0,ymax=1.5,xlabel=plastic strain $\varepsilon_p$,ylabel=normalised $\sigma_t$,xticklabels={,,},yticklabels={,,}]
\addplot[samples=100,red,very thick,fill=gray,fill opacity=0.5]{1.5*exp(-x)-.5*exp(-2*x)}\closedcycle;
\end{axis}
\end{tikzpicture}
\caption{tension}
\end{subfigure}\quad
\caption{example monotonic backbones used in the CDP model}\label{fig:cdp_backbone}
\end{figure}
Regularisations can be implemented by relating $a_\aleph$ and $b_\aleph$ with objective quantities. In specific, the model defines
\begin{gather}
g_\aleph=\int_0^\infty\sigma_\aleph~\md{\varepsilon_p}=\dfrac{f_\aleph}{b_\aleph}\left(1+\dfrac{a_\aleph}{2}\right)
\end{gather}
to be the area under the corresponding backbone. The specific fracture energy $G_f$ can be used to control the tension softening by further defining $g_t=G_f/l_c$ where $l_c$ is the characteristic length of the target element. The compression counterpart $g_c$ can be defined in a similar fashion. $g_t$ and $g_c$ are two main model parameters that provide mesh objective response. Details can be found elsewhere \citep{Lubliner1989}.

An isotropic damage model is adopted so the stress response is defined as
\begin{gather}
\mathbold{\sigma}=\left(1-D\right)\bar{\mathbold{\sigma}}=\left(1-D\right)\mathbold{E}\left(\mathbold{\varepsilon}-\mathbold{\varepsilon}_p\right)
\end{gather}
where $\bar{\mathbold{\sigma}}$ is the effective stress, $\mathbold{E}$ is the elastic stiffness, $D=1-\left(1-d_c\right)\left(1-sd_t\right)$ is a scalar degradation factor that relies on its uniaxial version $d_\aleph$, $s(\bar{\mathbold{\sigma}})$ is the stiffness recovery factor. The degradation factor $d_\aleph$, according to the original model \citep{Lee1998}, is
\begin{gather}
d_\aleph=1-\exp\left(-c_\aleph\varepsilon_p\right)
\end{gather}
where $c_\aleph$ is a material constant that controls the rate of degradation. By definition, $d_\aleph$ ranges from zero to unity.

To simplify the formulation, a normalised internal damage variable $\kappa_\aleph$ is adopted as a function of $\varepsilon_p$,
\begin{gather}
\kappa_\aleph=\dfrac{1}{g_\aleph}\int_0^{\varepsilon_p}\sigma_\aleph~\md{\varepsilon_p}.
\end{gather}
After some mathematical operations, $d_\aleph$ and $\sigma_\aleph$ can be expressed as functions of $\kappa_\aleph$. Hence, $\kappa_\aleph$ controls the developments of both damage and plasticity of the model. The evolution of $\kappa_\aleph$ is related to the ratios among three principal stress components. Tensile and compressive damage can evolve separately so that tension and compression backbones can have different hardening behaviour.

For numerical implementation, parameters $b_\aleph$ and $c_\aleph$ are associated with the reference degradation factors $\bar{D}_c$ at crush strength and $\bar{D}_t$ at \SI{50}{\percent} of the crack stress, respectively. 

The yield function is defined as
\begin{gather}
F=\alpha{}I_1+\sqrt{3J_2}+\beta\left\langle\hat\sigma_{1}\right\rangle-\left(1-\alpha\right)c,
\end{gather}
in which $I_1$ is the first invariant of stress, $J_2$ is the second invariant of deviatoric stress, $\alpha$ is a dimensionless constant, $\beta(\kappa_\aleph)$ and $c(\kappa_\aleph)$ are the cohesion related parameters, $\hat\sigma_{1}$ is the algebraic maximum eigenvalue of stress and $\left\langle\cdot\right\rangle$ is the Macaulay bracket.

A Drucker-Prager type function is used as the plastic potential,
\begin{gather}
G=\sqrt{2J_2}+\alpha_p{}I_1,
\end{gather}
where $\alpha_p$ is a material constant that controls dilatation.

Other recently proposed 3D concrete models, such as CDPM1 \citep{Grassl2006} and CDPM2 \citep{Grassl2013}, can also be used. However, some of these models may have difficulties in deriving the corresponding consistent tangent stiffness matrices. In those cases, some low rank update algorithms \citep[e.g.,][]{Shanno1970} can be adopted to obtain secant stiffness matrices. Noting that the state determination algorithm presented in Algorithm \ref{algo:adaptive_algorithm} is based on consistent tangent stiffness, a secant version can also be derived in the case of secant stiffness matrices.
\section{A Simple Concrete Model}\label{sec:backbone}
A simple biaxial concrete model based on uniaxial concrete models is constructed to illustrate an alternative in this section. A series of work existing in current literature is adopted for different parts of this simple model. The purpose of this section is to show a usable model for modelling in-plane behaviour of concrete, rather than to justify if it is more accurate.
\subsection{Biaxial Formulation}
Here a simple concrete model based on the fixed crack theory that resembles the one by \citet{Crisfield1989} is adopted. For any given strain $\mathbold{\varepsilon}=\begin{bmatrix}\varepsilon_x&\varepsilon_y&\gamma_{xy}\end{bmatrix}^\mT$ in the global coordinate system, it can be transformed into a local coordinate system by applying a rotation so that
\begin{gather}
\mathbold{\hat{\varepsilon}}=\mathbold{P}\mathbold{\varepsilon},
\end{gather}
where $\mathbold{\hat{\varepsilon}}=\begin{bmatrix}\varepsilon_{x'}&\varepsilon_{y'}&\gamma_{x'y'}\end{bmatrix}^\mT$. The transformation matrix $\mathbold{P}$ is can be expressed in terms of rotation angle $\theta$, that is
\begin{gather}
\mathbold{P}=\begin{bmatrix}
	\dfrac{1+\cos2\theta}{2} & \dfrac{1-\cos2\theta}{2} & \dfrac{\sin2\theta}{2}  \\[2mm]
	\dfrac{1-\cos2\theta}{2} & \dfrac{1+\cos2\theta}{2} & -\dfrac{\sin2\theta}{2} \\[2mm]
	-\sin2\theta             & \sin2\theta              & \cos2\theta
\end{bmatrix}.
\end{gather}
The angle $\theta$ equals the principal angle prior to the first yield, in which case $\gamma_{x'y'}=0$. Once either tensile or compressive strength is reached, $\theta$ is fixed in subsequent computation. The local quantities can be transformed back to the global coordinate system, for example,
\begin{gather}
\mathbold{\sigma}=\mathbold{P}^\mT\mathbold{\hat{\sigma}}.
\end{gather}
Accordingly, the conversion between two stiffness matrices can be expressed as
\begin{gather}
\mathbold{K}=\mathbold{P}^\mT\mathbold{\hat{K}}\mathbold{P}.
\end{gather}

For simplicity, there is no coupling between response along two orthogonal directions. So the yield surface in 2D space is a square (Rankine type). There are other models that incorporate Poisson's ratio in the formulation. If only one Poisson's ratio is used, the corresponding stiffness is not symmetric. It is also possible to treat cracked concrete as anisotropic material so that more than one Poisson's ratio could be adopted to better describe the post-crack behaviour. The stress and stiffness along each local direction is computed via the corresponding uniaxial concrete material model.
\subsection{Uniaxial Concrete Behaviour}
There are many uniaxial concrete models in current literature \citep[e.g.,][]{Kent1971,Popovics1973,Tsai1988,Chang1994}. In this work, Tsai's equation \citep{Tsai1988} is used as the uniaxial backbone for compression. It can be written as
\begin{gather}
\dfrac{\sigma}{f_c}=\dfrac{m}{1+\left(m-\dfrac{n}{n-1}\right)\dfrac{\varepsilon}{\varepsilon_c}+\dfrac{1}{n-1}\left(\dfrac{\varepsilon}{\varepsilon_c}\right)^n}\dfrac{\varepsilon}{\varepsilon_c},
\end{gather}
where $m$ and $n$ are two empirical parameters that control the pre-peak and post-peak shape of the curve respectively, following values are recommended by \citet{Tsai1988} for compression,
\begin{gather}
m=1+\dfrac{17.9}{f_c},\\n=\dfrac{f_c}{6.68}-1.85>1,
\end{gather}
with $f_c>0$ (\si{\mega\pascal}) and $\varepsilon_c>0$ denote the peak compressive stress and the corresponding strain, respectively. The tension backbone is defined in a similar fashion but with modified $m$ and $n$. \figref{fig:tsai} shows a cluster of backbones with different parameters.
\begin{figure}[htb]
\centering\footnotesize
\input{PIC/MAT/TSCM}
\caption{illustration of Tsai's equation}\label{fig:tsai}
\end{figure}

The residual strains along both directions are computed according to the empirical equations proposed by \citet{Chang1994}.
\begin{gather}
\varepsilon_{r}^+=\varepsilon_{un}^+-\dfrac{f_{un}^+\varepsilon_{un}^++0.67f_{un}^+\varepsilon_t}{f_{un}^++0.67E\varepsilon_t},\\
\varepsilon_{r}^-=\varepsilon_{un}^--\dfrac{f_{un}^-\varepsilon_{un}^-+0.57f_{un}^-\varepsilon_c}{f_{un}^-+0.57E\varepsilon_c},
\end{gather}
where superscripts $^+$ and $^-$ denote tension and compression respectively, $f_{un}$ and $\varepsilon_{un}$ is the maximum unload stress and strain, $\varepsilon_t$ and $\varepsilon_c$ are the crack and peak strains. The unloading/reloading behaviour is assumed to be linear. Whenever load reversal occurs, the response varies linearly between the corresponding residual strain point and the point that corresponds to a given level of unloading stress on the opposing unloading path.

\begin{figure}[htb]
\centering\footnotesize
\input{PIC/MAT/TSAI}
\caption{illustration of hysteresis rule}\label{fig:hysteresis}
\end{figure}
\figref{fig:hysteresis} shows an illustration of the adopted hysteresis rule. The peak stress is chosen to be close to the crack stress so that the hysteresis loop can be better seen. It thus does not correspond to any real concrete behaviour. Other more complex hysteresis rules can be applied, although in which small cycle behaviour can be carefully treated.
\subsection{Shear Response}
Due to the lack of a proper nonlinear shear response, a bilinear elastic relationship can be defined. For the hardening branch, a shear retention factor $\beta$ can be adopted so the hardening modulus can be defined as $\beta{}G$. The corresponding yielding shear stress can be limited to a user-defined value that may be associated with the tensile strength.
\section{Reinforced Concrete}
For reinforced concrete, there are many in-plane models available, including well-known ones such as MCFT \citep{Vecchio1986}, CSMM \citep{Hsu2002} and their variants. The basic strategy adopted by those models is to decompose total strain either in the principal space (for a rotating crack theory) or along a fixed direction (for a fixed crack theory). Each strain component can be further split into different portions to account for various effects. Reinforcement is included in a smeared manner. Local phenomena such as aggregate locking, stress transition/concentration and dowel action, can also be considered.

However, due to that many models (MCFT, DSFM \citep{Vecchio2000}, FA-STM, etc.) perform strain decompositions and use stress equilibrium as the governing equation, iterations are required to compute trial state and often only the secant stiffness is available. The overall state updating algorithm has a very slow convergence rate (sub-linear) and often faces difficulties in the unloading stage. For example, MCFT (at least the original version) does not define any unloading behaviour and thus can only be used in analyses under monotonic loading.

In this work, reinforcement is modelled independently in a smeared approach. Hence interactions between reinforcement and host concrete, such as dowel action, bar buckling and confinement, are not considered (not in an discrete way but still can be accounted for by modifying material models). The total material stiffness $\mathbold{D}$ and stress $\mathbold{\sigma}$ can be expressed as the superposition of concrete and reinforcement response.
\begin{gather}
\mathbold{D}=\mathbold{D}_c+\mathbold{D}_s,\quad
\mathbold{\sigma}=\mathbold{\sigma}_c+\mathbold{\sigma}_s,
\end{gather}
where $\mathbold{D}$ is the overall material stiffness, subscripts $_c$ and $_s$ denote concrete and reinforcement portion, respectively. $\mathbold{D}_s$ is often assumed to be orthogonal so that
\begin{gather}
\mathbold{D}_s=\begin{bmatrix}
	\rho_xE_x & 0         & 0 \\
	0         & \rho_yE_y & 0 \\
	0         & 0         & 0
\end{bmatrix},\quad
\mathbold{\sigma}_s=\begin{bmatrix}
	\rho_x\sigma_x \\
	\rho_y\sigma_y \\
	0
\end{bmatrix},
\end{gather}
where $\rho_x$ and $\rho_y$ are two reinforcement ratios along two axes, $E_x$ and $E_y$ are corresponding steel moduli. It shall be stressed that, although the smeared approach is used, GCMQ itself does not impose any constraint on the implementation of reinforcement. The discrete approach, or a combined method, could be employed as well.
\EoC