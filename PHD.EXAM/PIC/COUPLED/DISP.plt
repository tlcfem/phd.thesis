set term tikz size 13.5cm,4cm
set key Left right bottom at screen .975,.05 spacing 1 maxrow 5 width 2 opaque samplen 1
set xrange [0:40]
set xlabel "time (\\si{\\second})"
set ylabel "roof displacement (\\si{\\milli\\metre})"
set xtics nomirror in 0,5,40
set yrange [-20:30]
set ytics nomirror in -20,10,30
set margins 0,0,0,0
set grid
set border 15
set output "DISP.tex"
plot "DISP" every 2 using 1:($2)*1000 w lp pointinterval 199 lw 2 lc rgb "#e41a1c" title "SGCMQI",\
     "DISP" every 2 using 1:($3)*1000 w lp pointinterval 211 lw 2 lc rgb "#377eb8" title "SGCMQL",\
     "DISP" every 2 using 1:($4)*1000 w lp pointinterval 223 lw 2 lc rgb "#4daf4a" title "SGCMQG"
set output