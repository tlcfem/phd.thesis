set term tikz size 6cm,3.5cm
set key Left left top at screen .05,.95 spacing 1 maxrow 5 width -1 opaque
set xrange [1E-3:1E3]
set logscale x
set xlabel "moduli ratio ($E_b/E_p$)"
set ylabel "force (\\num{E-3})"
# set xtics nomirror in 0,2,10
set yrange [0:5]
set ytics nomirror in 1,1,4
set margins 0,0,0,0
set grid
set border 15
set output "TEST.tex"
plot "TEST" every 1 using 1:($2)*1000 w lp pointinterval 3 lw 2 lc rgb "#e41a1c" title "reference",\
     "TEST" every 1 using 1:($3)*1000 w lp pointinterval 5 lw 2 lc rgb "#377eb8" title "\\num{2x2} mesh",\
     "TEST" every 1 using 1:($4)*1000 w lp pointinterval 7 lw 2 lc rgb "#4daf4a" title "\\num{4x4} mesh"
set output
set output "ERROR.tex"
set ylabel "error (\\si{\\percent})"
set yrange [-100:10]
set ytics nomirror in -80,20,10
set key Left right top at screen .95,.95 spacing 1 maxrow 5 width -1 opaque
plot "TEST" every 1 using 1:5 w lp pointinterval 5 lw 2 lc rgb "#377eb8" title "\\num{2x2} mesh",\
     "TEST" every 1 using 1:6 w lp pointinterval 7 lw 2 lc rgb "#4daf4a" title "\\num{4x4} mesh"
set output