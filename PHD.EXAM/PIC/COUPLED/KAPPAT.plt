set term tikz size 12cm,4cm
set key Left right bottom at screen .975,.1 spacing 1 maxrow 5 width -3 opaque
set xrange [0:40]
set xlabel "time (\\si{\\second})"
set y2label "roof displacement (\\si{\\milli\\metre})"
set ylabel "tensile damage index ($\\kappa_t$)"
set xtics nomirror in 0,5,40
set y2range [-20:30]
set yrange [0:1]
set y2tics nomirror in -20,10,30
set ytics nomirror in 0,.2,1
set margins 0,0,0,0
set grid
set border 15
set output "KAPPAT.tex"
plot "DISP" every 2 using 1:($2)*1000 w l lw 2 lc rgb "#aaaaaa" axes x1y2 title "response history",\
     "KAPPAT" every 2 using 1:2 w lp pointinterval 199 lw 3 lc rgb "#e41a1c" title "left wall left corner",\
     "KAPPAT" every 2 using 1:3 w lp pointinterval 227 lw 3 lc rgb "#377eb8" title "left wall right corner",\
     "KAPPAT" every 2 using 1:4 w lp pointinterval 211 lw 3 lc rgb "#4daf4a" title "right wall left corner",\
     "KAPPAT" every 2 using 1:5 w lp pointinterval 223 lw 3 lc rgb "#984ea3" title "right wall right corner"
set output