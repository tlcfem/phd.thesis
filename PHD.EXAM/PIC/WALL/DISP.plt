set term tikz size 9cm,4cm
set key Left right bottom at screen .975,.05 spacing 1 maxrow 5 width -3 samplen 1
set xrange [0:40]
set xlabel "time (\\si{\\second})"
set ylabel "roof displacement (\\si{\\milli\\metre})"
set xtics nomirror in 0,5,40
set yrange [-35:55]
set ytics nomirror in -20,20,80
set margins 0,0,0,0
set grid
set border 15
set output "DISP.tex"
plot "DISP" every 2 using 1:($5)*1000 w lp pointinterval 227 lw 2 lc rgb "#000000" title "linear response",\
     "DISP" every 2 using 1:($2)*1000 w lp pointinterval 199 lw 2 lc rgb "#4daf4a" title "SGCMQI(L)",\
     "DISP" every 2 using 1:($4)*1000 w lp pointinterval 223 lw 2 lc rgb "#e41a1c" title "SGCMQG"
set output
set key Left right bottom at screen .975,.1 spacing 1 width -1 samplen 1
set yrange [-25:50]
set output "MESH.tex"
plot "DISP" every 2 using 1:($2)*1000 w lp pointinterval 199 lw 2 lc rgb "#e41a1c" title "\\num{2x2} SGCMQI(L)",\
     "DISP" every 2 using 1:($4)*1000 w lp pointinterval 211 lw 2 lc rgb "#377eb8" title "\\num{2x2} SGCMQG",\
     "MESH" every 1 using 1:($2)*1000 w lp pointinterval 223 lw 2 lc rgb "#4daf4a" title "\\num{3x3} SGCMQI(L)",\
     "MESH" every 1 using 1:($3)*1000 w lp pointinterval 227 lw 2 lc rgb "#984ea3" title "\\num{3x3} SGCMQG"
set output