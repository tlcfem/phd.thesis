set term tikz size 6.5cm,4cm
set key Left right bottom at screen .95,.1 spacing 1 maxrow 5 width -7 samplen .75
set xrange [-2:12]
set xlabel "strain (\\num{E-3})"
set ylabel "stress (\\si{\\mega\\pascal})" offset .5,0
set xtics nomirror in 0,2,10
set yrange [-80:380]
set ytics nomirror in 0,100,300
set margins 0,0,0,0
set grid
set border 15
set output "REBAR.tex"
plot "REBAR" every 2 using ($1)*1000:2 w lp pointinterval 199 lw 2 lc rgb "#e41a1c" title "left corner",\
     "REBAR" every 2 using ($3)*1000:4 w lp pointinterval 227 lw 2 lc rgb "#377eb8" title "right corner"
set output
unset key
set xrange [-30:45]
set xlabel "roof displacement (\\si{\\milli\\metre})"
set ylabel "base resistance (\\si{\\kilo\\newton})" offset .5,0
set xtics nomirror in -20,10,40
set yrange [-380:380]
set ytics nomirror in -300,150,300
set output "SHEAR.tex"
plot "SHEAR" every 1 using ($1)*1000:($2)*1000 w l lw 2 lc rgb "#e41a1c" notitle
set output