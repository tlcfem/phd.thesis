set term tikz size 4cm,2cm
set key Left right bottom at screen .95,.05 spacing 1 samplen 1 width -3 maxrow 5
set xrange [0:2]
set xlabel "time (\\si{\\second})" offset 0,.5
set ylabel "tip displacement (\\si{\\milli\\metre})" offset 1.5,0
set xtics nomirror in 0,.5,2
set yrange [-160:90]
set ytics nomirror in -150,50,50
set margins 0,0,0,0
set grid
set border 15
set output "LINEAR1.tex"
plot "CANTI1" using 1:($2)*1000 w lp pointinterval 17 lw 2 lt 3 lc rgb "#e41a1c" title "(S)GCMQ",\
     "CANTI1" using 1:($8)*1000 w lp pointinterval 19 lw 2 lt 4 lc rgb "#377eb8" title "Q4"
set output
set output "LINEAR2.tex"
plot "CANTI2" using 1:($2)*1000 w lp pointinterval 17 lw 2 lt 3 lc rgb "#e41a1c" title "(S)GCMQ",\
     "CANTI2" using 1:($8)*1000 w lp pointinterval 19 lw 2 lt 4 lc rgb "#377eb8" title "Q4"
set output
set output "LINEAR4.tex"
plot "CANTI4" using 1:($2)*1000 w lp pointinterval 17 lw 2 lt 3 lc rgb "#e41a1c" title "(S)GCMQ",\
     "CANTI4" using 1:($8)*1000 w lp pointinterval 19 lw 2 lt 4 lc rgb "#377eb8" title "Q4"
set output
set output "LINEAR16.tex"
plot "CANTI16" using 1:($2)*1000 w lp pointinterval 17 lw 2 lt 3 lc rgb "#e41a1c" title "(S)GCMQ",\
     "CANTI16" using 1:($8)*1000 w lp pointinterval 19 lw 2 lt 4 lc rgb "#377eb8" title "Q4"
set output