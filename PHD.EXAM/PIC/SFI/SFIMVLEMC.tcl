model basic -ndm 2 -ndf 3

set E 1
set V [expr 1./3.]
set AR 4
set L [expr 2.*$AR]
set NUM 16
set C .5

nDMaterial ElasticIsotropic 1 $E $V
nDMaterial PlaneStress 2 1

node 1 0 0
node 2 0 $L

node 3 0 [expr $L/4.]
node 4 0 [expr $L/4.*2.]
node 5 0 [expr $L/4.*3.]
element SFI_MVLEM 1 1 3 $NUM $C -thick 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 -width .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 -mat 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
element SFI_MVLEM 2 3 4 $NUM $C -thick 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 -width .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 -mat 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
element SFI_MVLEM 3 4 5 $NUM $C -thick 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 -width .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 -mat 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
element SFI_MVLEM 4 5 2 $NUM $C -thick 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 -width .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 -mat 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2


fix 1 1 1 1

pattern Plain 1 Linear {
   load 2 1 0 0
}

system BandSPD
constraints Plain
integrator LoadControl 1
test NormDispIncr 1E-4 100
algorithm Newton
numberer RCM
analysis Static
analyze 1

print node 2