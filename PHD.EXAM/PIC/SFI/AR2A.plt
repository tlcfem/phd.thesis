reset
set term tikz size 4cm,3cm
set output "AR2A.tex"
set key samplen .75 spacing .75 width -2 right top at screen .95,.95 maxrows 3
set logscale x 2
set xrange [2:32]
set yrange [-20:50]
set xlabel "number of transverse fibres"
set ylabel "tip deflection error (\\si{\\percent})" offset .5,0
set xtics 2
set ytics 10
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "AR2A" using 1:2 w linespoints lw 2 pt 1 lc rgb "#d7191c" title "$c=0.30$",\
     "AR2A" using 1:3 w linespoints lw 2 pt 2 lc rgb "#fdae61" title "$c=0.35$",\
     "AR2A" using 1:4 w linespoints lw 2 pt 3 lc rgb "#ffffbf" title "$c=0.40$",\
     "AR2A" using 1:5 w linespoints lw 2 pt 4 lc rgb "#abd9e9" title "$c=0.45$",\
     "AR2A" using 1:6 w linespoints lw 2 pt 5 lc rgb "#2c7bb6" title "$c=0.50$"
set output