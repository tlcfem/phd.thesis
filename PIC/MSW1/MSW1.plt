set term tikz size 6.5cm,4cm
set output "MSW1.tex"
set key Left right top at screen .975,.925 samplen 1 width 1
set xrange [0:39.6]
set x2range [0:2.2]
set yrange [0:2.2]
set xlabel "top displacement (\\si{\\milli\\metre})"
set x2label "drift ratio (\\si{\\percent})"
set ylabel "base resistance (\\SI{E2}{\\kilo\\newton})"
set xtics nomirror in 0,9,27
set x2tics nomirror in 0,.5,1.5
set ytics nomirror in 0,1,2
set mytics 2
set margins 0,0,0,0
unset grid
set border 15
plot "MSW1-EXP" using 1:($2)/100 w l lw 2 lc rgb "#999999" title "MSW1",\
     "MSW1-2X2" using 1:($2)/100000 w lp pointinterval 11 lw 3 lc rgb "#e41a1c" title "SGCMQI",\
     "MSW1-2X2" using 1:($3)/100000 w lp pointinterval 17 lw 3 lc rgb "#377eb8" title "SGCMQL",\
     "MSW1-2X2" using 1:($4)/100000 w lp pointinterval 23 lw 3 lc rgb "#4daf4a" title "SGCMQG"
set output