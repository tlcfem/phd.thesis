set term tikz size 6cm,4cm
set key Left right top at screen .95,.95 spacing 1 samplen 2 width 1.5 maxrow 5 opaque
set xrange [0:100]
set xlabel "$\\delta{}_V$ (\\si{\\micro\\metre})"
set ylabel "resistance (\\si{\\kilo\\newton})"
set xtics nomirror in 0,20,100
set yrange [0:22]
set ytics nomirror in 0,5,20
set margins 0,0,0,0
set grid
set border 15
set output "RESPONSE.tex"
plot "RESPONSE" using ($1)*1000:($2)/1000 w lp pointinterval 17 lw 2 lt 3 lc rgb "#e41a1c" title "coarse",\
"RESPONSE" using ($3)*1000:($4)/1000 w lp pointinterval 11 lw 2 lt 4 lc rgb "#377eb8" title "medium",\
"RESPONSE" using ($5)*1000:($6)/1000 w lp pointinterval 23 lw 2 lt 5 lc rgb "#4daf4a" title "fine"
set output