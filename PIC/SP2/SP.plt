set term tikz size 6cm,4cm
set key Left noopaque right bottom at screen .9,.15 maxrow 3 spacing 1 width 1 samplen 1
set xrange [0:2]
set yrange [0:1.4]
set xlabel "top displacement (\\num{E-3})"
set ylabel "base resistance"
set xtics nomirror in
set ytics nomirror in 0,.5,1
set mxtics 2
set mytics 2
set margins 0,0,0,0
set grid
set border 15
set output "SP-1X1.tex"
plot "DISP-1X1" using ($1)*1000:2 w lp pointinterval 11 lw 2 lc rgb "#e41a1c" title "GCMQI",\
     "DISP-1X1" using ($1)*1000:3 w lp pointinterval 17 lw 2 lc rgb "#e41a1c" title "GCMQL",\
     "DISP-1X1" using ($1)*1000:4 w lp pointinterval 23 lw 2 lc rgb "#e41a1c" title "GCMQG",\
     "DISP-1X1" using ($1)*1000:5 w lp pointinterval 31 lw 2 lc rgb "#377eb8" title "SGCMQI",\
     "DISP-1X1" using ($1)*1000:6 w lp pointinterval 11 lw 2 lc rgb "#377eb8" title "SGCMQL",\
     "DISP-1X1" using ($1)*1000:7 w lp pointinterval 17 lw 2 lc rgb "#377eb8" title "SGCMQG"
set output
set output "SP-2X2.tex"
plot "DISP-2X2" using ($1)*1000:2 w lp pointinterval 11 lw 2 lc rgb "#e41a1c" title "GCMQI",\
     "DISP-2X2" using ($1)*1000:3 w lp pointinterval 17 lw 2 lc rgb "#e41a1c" title "GCMQL",\
     "DISP-2X2" using ($1)*1000:4 w lp pointinterval 23 lw 2 lc rgb "#e41a1c" title "GCMQG",\
     "DISP-2X2" using ($1)*1000:5 w lp pointinterval 31 lw 2 lc rgb "#377eb8" title "SGCMQI",\
     "DISP-2X2" using ($1)*1000:6 w lp pointinterval 11 lw 2 lc rgb "#377eb8" title "SGCMQL",\
     "DISP-2X2" using ($1)*1000:7 w lp pointinterval 17 lw 2 lc rgb "#377eb8" title "SGCMQG"
set output
set output "SP-4X4.tex"
plot "DISP-4X4" using ($1)*1000:2 w lp pointinterval 11 lw 2 lc rgb "#e41a1c" title "GCMQI",\
     "DISP-4X4" using ($1)*1000:3 w lp pointinterval 17 lw 2 lc rgb "#e41a1c" title "GCMQL",\
     "DISP-4X4" using ($1)*1000:4 w lp pointinterval 23 lw 2 lc rgb "#e41a1c" title "GCMQG",\
     "DISP-4X4" using ($1)*1000:5 w lp pointinterval 31 lw 2 lc rgb "#377eb8" title "SGCMQI",\
     "DISP-4X4" using ($1)*1000:6 w lp pointinterval 11 lw 2 lc rgb "#377eb8" title "SGCMQL",\
     "DISP-4X4" using ($1)*1000:7 w lp pointinterval 17 lw 2 lc rgb "#377eb8" title "SGCMQG"
set output
set output "SP-8X8.tex"
plot "DISP-8X8" using ($1)*1000:2 w lp pointinterval 11 lw 2 lc rgb "#e41a1c" title "GCMQI",\
     "DISP-8X8" using ($1)*1000:3 w lp pointinterval 17 lw 2 lc rgb "#e41a1c" title "GCMQL",\
     "DISP-8X8" using ($1)*1000:4 w lp pointinterval 23 lw 2 lc rgb "#e41a1c" title "GCMQG",\
     "DISP-8X8" using ($1)*1000:5 w lp pointinterval 31 lw 2 lc rgb "#377eb8" title "SGCMQI",\
     "DISP-8X8" using ($1)*1000:6 w lp pointinterval 11 lw 2 lc rgb "#377eb8" title "SGCMQL",\
     "DISP-8X8" using ($1)*1000:7 w lp pointinterval 17 lw 2 lc rgb "#377eb8" title "SGCMQG"
set output