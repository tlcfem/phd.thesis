set term tikz size 6.5cm,4cm
set output "J2-1X1.tex"
set key Left noopaque right bottom at screen .95,.25 maxrow 3 spacing 1 width 1
set xrange [0:2]
set yrange [0:2.5]
set xlabel "top displacement (\\num{E-3})"
set ylabel "base resistance (\\si{\\mega\\newton})"
set xtics nomirror in
set ytics nomirror in 0,1,2
set mxtics 2
set mytics 2
set margins 0,0,0,0
set grid
set border 15
plot "J2" using ($1)*1000:2 w lp pointinterval 11 lw 2 lc rgb "#e41a1c" title "SGCMQI",\
     "J2" using ($1)*1000:3 w lp pointinterval 17 lw 2 lc rgb "#377eb8" title "SGCMQL",\
     "J2" using ($1)*1000:4 w lp pointinterval 23 lw 2 lc rgb "#4daf4a" title "SGCMQG"
set output
set output "J2-2X2.tex"
plot "J2" using ($1)*1000:5 w lp pointinterval 11 lw 2 lc rgb "#e41a1c" title "SGCMQI",\
     "J2" using ($1)*1000:6 w lp pointinterval 17 lw 2 lc rgb "#377eb8" title "SGCMQL",\
     "J2" using ($1)*1000:7 w lp pointinterval 23 lw 2 lc rgb "#4daf4a" title "SGCMQG"
set output