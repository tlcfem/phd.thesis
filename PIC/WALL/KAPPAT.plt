set term tikz size 12cm,4cm
set key Left right bottom at screen .975,.1 spacing 1 maxrow 5 width -3 opaque
set xrange [0:30]
set xlabel "time (\\si{\\second})"
set y2label "roof displacement (\\si{\\milli\\metre})"
set ylabel "tensile damage index ($\\kappa_t$)"
set xtics nomirror in 0,5,40
set y2range [-25:45]
set yrange [0:1]
set y2tics nomirror in -20,20,40
set ytics nomirror in 0,.2,1
set margins 0,0,0,0
set grid
set border 15
set output "KAPPAT.tex"
plot "DISP" every 2 using 1:($2)*1000 w l lw 2 lc rgb "#aaaaaa" axes x1y2 title "response history",\
     "KAPPAT" every 2 using 1:2 w lp pointinterval 199 lw 3 lc rgb "#e41a1c" title "left bottom corner",\
     "KAPPAT" every 2 using 1:3 w lp pointinterval 227 lw 3 lc rgb "#377eb8" title "right bottom corner"
set output