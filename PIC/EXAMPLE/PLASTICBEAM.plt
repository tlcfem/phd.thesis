reset
set term tikz size 6cm,4cm
set output "PLASTICBEAM.tex"
set key samplen 1 spacing 1.1 width 1 center at 7,.5 maxrows 7
set xrange [0:10]
set yrange [0:1.6]
set xlabel "tip deflection (\\num{E-2})"
set ylabel "load"
set xtics 2
set ytics .5
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "PLASTICBEAM" using ($4*100):1 w linespoints lw 2 pt 1 ps 1 lc rgb "#d73027" title "GCMQ-I",\
     "PLASTICBEAM" using ($3*100):1 w linespoints lw 2 pt 2 ps 1 lc rgb "#fc8d59" title "GCMQ-L",\
     "PLASTICBEAM" using ($2*100):1 w linespoints lw 2 pt 3 ps 1 lc rgb "#4575b4" title "GCMQ-G"
set output