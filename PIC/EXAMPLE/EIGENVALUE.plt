reset
set term tikz size 6cm,4cm
set output "EIGENVALUE.tex"
set key Left samplen 1 spacing 1 width 1 center at .15,3.5 maxrows 6
set xrange [0:.5]
set yrange [0:5]
set xlabel "$\\nu$"
set ylabel "normalized eigenvalue"
set xtics .05
set ytics 1
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "EIGENVALUE" using 1:2 w linespoints lw 2 pt 1 lc rgb "#1b9e77" title "mode 5 6",\
     "EIGENVALUE" using 1:3 w linespoints lw 2 pt 2 lc rgb "#d95f02" title "mode 7",\
     "EIGENVALUE" using 1:4 w linespoints lw 2 pt 3 lc rgb "#7570b3" title "mode 8 9",\
     "EIGENVALUE" using 1:5 w linespoints lw 2 pt 4 lc rgb "#e7298a" title "mode 10",\
     "EIGENVALUE" using 1:6 w linespoints lw 2 pt 5 lc rgb "#66a61e" title "mode 11",\
     "EIGENVALUE" using 1:7 w linespoints lw 2 pt 6 lc rgb "#e6ab02" title "mode 12"
set output