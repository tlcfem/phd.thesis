reset
set term tikz size 6cm,4cm
set output "EIGENVALUE3.tex"
set key samplen 1 spacing 1 width 1 center at 3.5,2.5 maxrows 6
set xrange [0:5]
set yrange [0:10]
set xlabel "bulk modulus"
set ylabel "normalized eigenvalue"
set xtics 1
set ytics 2.5
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "EIGENVALUE" using (1/(3-6*$1)):7 w linespoints lw 2 pt 6 lc rgb "#e6ab02" title "mode 12"
set output