reset
set term tikz size 6cm,4cm
set output "RATE.tex"
set key Left samplen 1 spacing 1.1 width 1 center at 10000,1 maxrows 4
set logscale x 10
set logscale y 10
set xrange [10:100000]
set yrange [0.0005:5]
set xlabel "number of DoFs"
set ylabel "vertical resistance error \\si{\\percent}"
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "RATE" using 1:2 w linespoints lw 2 pt 1 lc rgb "#d7191c" title "(S)GCMQ
set output