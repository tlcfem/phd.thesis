reset
set term tikz size 6cm,4cm
set output "PLATE-DISP.tex"
set key Left samplen 1 spacing 1 width 0 left top at screen .05,.95 maxrows 6
set xrange [0:30]
set yrange [0:35]
set xlabel "horizontal displacement (\\num{E-3})"
set ylabel "horizontal resistance"
set xtics 5
set ytics 0,5,25
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "PLATE-DISP" using ($1)*1000:2 w lp pointinterval 11 lw 2 lc rgb "#e41a1c" title "\\scriptsize{}SGCMQ coarse",\
     "PLATE-DISP" using ($1)*1000:8 w lp pointinterval 17 lw 2 lc rgb "#377eb8" title "\\scriptsize{}SGCMQ medium",\
     "PLATE-DISP" using ($1)*1000:14 w lp pointinterval 23 lw 2 lc rgb "#4daf4a" title "\\scriptsize{}SGCMQ fine",\
     "PLATE-DISP" using ($1)*1000:5 w lp pointinterval 13 lw 2 lc rgb "#984ea3" title "\\scriptsize{}GCMQ coarse",\
     "PLATE-DISP" using ($1)*1000:11 w lp pointinterval 19 lw 2 lc rgb "#ff7f00" title "\\scriptsize{}GCMQ medium",\
     "PLATE-DISP" using ($1)*1000:17 w lp pointinterval 29 lw 2 lc rgb "#ffff33" title "\\scriptsize{}GCMQ fine"
set output