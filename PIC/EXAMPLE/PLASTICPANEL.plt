reset
set term tikz size 3.2cm,4.5cm
set output "PLASTICPANELC.tex"
set key samplen 1 spacing 1.1 width 1 center at 0.1,.4 maxrows 7
set xrange [0:.2]
set yrange [0:1.2]
set xlabel "displacement"
set ylabel "resistance" offset 1,0
set xtics .1
set ytics .5
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "PLASTICPANEL" using 1:13 w l lw 3 lc rgb "#d7191c" title "Q4",\
     "PLASTICPANEL" using 1:10 w l lw 3 lc rgb "#fdae61" title "-I",\
     "PLASTICPANEL" using 1:11 w l lw 3 lc rgb "#abdda4" title "-L",\
     "PLASTICPANEL" using 1:12 w l lw 3 lc rgb "#2b83ba" title "-G"
set output
set output "PLASTICPANELB.tex"
set key samplen 1 spacing 1.1 width 1 center at 0.1,.4 maxrows 7
set xrange [0:.2]
set yrange [0:1.2]
set xlabel "displacement"
set ylabel "resistance" offset 1,0
set xtics .1
set ytics .5
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "PLASTICPANEL" using 1:9 w l lw 3 lc rgb "#d7191c" title "Q4",\
     "PLASTICPANEL" using 1:6 w l lw 3 lc rgb "#fdae61" title "-I",\
     "PLASTICPANEL" using 1:7 w l lw 3 lc rgb "#abdda4" title "-L",\
     "PLASTICPANEL" using 1:8 w l lw 3 lc rgb "#2b83ba" title "-G"
set output
set output "PLASTICPANELA.tex"
set key samplen 1 spacing 1.1 width 1 center at 0.1,.6 maxrows 7
set xrange [0:.2]
set yrange [0:1.8]
set xlabel "displacement"
set ylabel "resistance" offset 1,0
set xtics .1
set ytics .5
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "PLASTICPANEL" using 1:5 w l lw 3 lc rgb "#d7191c" title "Q4",\
     "PLASTICPANEL" using 1:2 w l lw 3 lc rgb "#fdae61" title "-I",\
     "PLASTICPANEL" using 1:3 w l lw 3 lc rgb "#abdda4" title "-L",\
     "PLASTICPANEL" using 1:4 w l lw 3 lc rgb "#2b83ba" title "-G"
set output