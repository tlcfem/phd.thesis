reset
set term tikz size 3.2cm,4.5cm
set output "PLASTICPLATEA.tex"
set key samplen 1 spacing 1.1 width 1 center at 0.015,-5 maxrows 7
set xrange [-.04:.04]
set yrange [-10:10]
set xlabel "displacement"
set ylabel "resistance" offset 1,0
set xtics .02
set ytics 5
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "PLASTICPLATE4" using 1:2 w points lw 2 pt 1 ps 1 lc rgb "#d7191c" title "Q4",\
     "PLASTICPLATE4" using 1:3 w points lw 2 pt 2 ps 1 lc rgb "#fdae61" title "-I",\
     "PLASTICPLATE4" using 1:4 w points lw 2 pt 3 ps 1 lc rgb "#abdda4" title "-L",\
     "PLASTICPLATE4" using 1:5 w points lw 2 pt 4 ps 1 lc rgb "#2b83ba" title "-G"
set output
set output "PLASTICPLATEB.tex"
set key samplen 1 spacing 1.1 width 1 center at 0.015,-5 maxrows 7
set xrange [-.04:.04]
set yrange [-10:10]
set xlabel "displacement"
set ylabel "resistance" offset 1,0
set xtics .02
set ytics 5
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "PLASTICPLATE8" using 1:2 w points lw 2 pt 1 ps 1 lc rgb "#d7191c" title "Q4",\
     "PLASTICPLATE8" using 1:3 w points lw 2 pt 2 ps 1 lc rgb "#fdae61" title "-I",\
     "PLASTICPLATE8" using 1:4 w points lw 2 pt 3 ps 1 lc rgb "#abdda4" title "-L",\
     "PLASTICPLATE8" using 1:5 w points lw 2 pt 4 ps 1 lc rgb "#2b83ba" title "-G"
set output
set output "PLASTICPLATEC.tex"
set key samplen 1 spacing 1.1 width 1 center at 0.015,-5 maxrows 7
set xrange [-.04:.04]
set yrange [-10:10]
set xlabel "displacement"
set ylabel "resistance" offset 1,0
set xtics .02
set ytics 5
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "PLASTICPLATE16" using 1:2 w points lw 2 pt 1 ps 1 lc rgb "#d7191c" title "Q4",\
     "PLASTICPLATE16" using 1:3 w points lw 2 pt 2 ps 1 lc rgb "#fdae61" title "-I",\
     "PLASTICPLATE16" using 1:4 w points lw 2 pt 3 ps 1 lc rgb "#abdda4" title "-L",\
     "PLASTICPLATE16" using 1:5 w points lw 2 pt 4 ps 1 lc rgb "#2b83ba" title "-G"
set output