reset
set term tikz size 6cm,4cm
set output "EIGENVALUE2.tex"
set key samplen 1 spacing 1 width 1 center at .45,.7 maxrows 6
set xrange [.333333:.5]
set yrange [.6:1]
set xlabel "shear modulus"
set ylabel "normalized eigenvalue"
set xtics .05
set ytics .1
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "EIGENVALUE" using (.5/($1+1)):5 w linespoints lw 2 pt 4 lc rgb "#e7298a" title "mode 10"
set output