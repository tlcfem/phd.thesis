set term tikz size 6cm,3.5cm
set key Left left top at screen .05,.95 spacing 1 samplen 2 width 1.5 maxrow 5 opaque
set xrange [0:2]
set xlabel "time (\\si{\\second})"
set ylabel "tip displacement (\\si{\\milli\\metre})"
set xtics nomirror in 0,.5,2
set yrange [-200:150]
set ytics nomirror in -200,50,100
set margins 0,0,0,0
set grid
set border 15
set output "NONLINEAR4.tex"
plot "NCANTI4" using 1:($2)*1000 w lp pointinterval 17 lw 2 lt 3 lc rgb "#e41a1c" title "(S)GCMQI",\
     "NCANTI4" using 1:($3)*1000 w lp pointinterval 19 lw 2 lt 4 lc rgb "#377eb8" title "(S)GCMQL",\
     "NCANTI4" using 1:($4)*1000 w lp pointinterval 23 lw 2 lt 5 lc rgb "#4daf4a" title "(S)GCMQG",\
     "NCANTI4" using 1:($8)*1000 w lp pointinterval 29 lw 2 lt 6 lc rgb "#984ea3" title "Q4"
set output
set output "NONLINEAR8.tex"
plot "NCANTI8" using 1:($2)*1000 w lp pointinterval 17 lw 2 lt 3 lc rgb "#e41a1c" title "(S)GCMQI",\
     "NCANTI8" using 1:($3)*1000 w lp pointinterval 19 lw 2 lt 4 lc rgb "#377eb8" title "(S)GCMQL",\
     "NCANTI8" using 1:($4)*1000 w lp pointinterval 23 lw 2 lt 5 lc rgb "#4daf4a" title "(S)GCMQG",\
     "NCANTI8" using 1:($8)*1000 w lp pointinterval 29 lw 2 lt 6 lc rgb "#984ea3" title "Q4"
set output