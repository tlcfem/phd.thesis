set term tikz size 13.5cm,4cm
set key Left right bottom at screen .975,.05 spacing 1 maxrow 5 width 2 opaque samplen 1
set xrange [0:40]
set xlabel "time (\\si{\\second})"
set ylabel "axial force (\\si{\\kilo\\newton})"
set xtics nomirror in 0,5,40
set yrange [-230:120]
set ytics nomirror in -200,50,100
set margins 0,0,0,0
set grid
set border 15
set output "AXIAL.tex"
plot "AXIAL" every 2 using 1:($2)*-1000 w lp pointinterval 199 lw 2 lc rgb "#e41a1c" title "left wall",\
     "AXIAL" every 2 using 1:($3)*-1000 w lp pointinterval 211 lw 2 lc rgb "#377eb8" title "right wall",\
     "AXIAL" every 2 using 1:($2)*-1000-($3)*1000 w lp pointinterval 223 lw 2 lc rgb "#4daf4a" title "summation"
set output