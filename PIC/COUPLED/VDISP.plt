set term tikz size 6cm,3.5cm
set key Left left top at screen .05,.95 spacing 1 maxrow 5 width -2 opaque
set xrange [-20:30]
set xlabel "horizontal displacement (\\si{\\milli\\metre})"
set ylabel "vertical displacement (\\si{\\milli\\metre})"
set xtics nomirror in -20,10,30
set yrange [0:6]
set ytics nomirror in 0,2,6
set margins 0,0,0,0
set grid
set border 15
set output "LVDISP.tex"
plot "VDISP" every 2 using ($1)*1000:($2)*1000 w lp pointinterval 199 lw 2 lc rgb "#e41a1c" title "left wall"
set output
set output "RVDISP.tex"
plot "VDISP" every 2 using ($3)*1000:($4)*1000 w lp pointinterval 199 lw 2 lc rgb "#377eb8" title "right wall"
set output