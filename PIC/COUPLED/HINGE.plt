set term tikz size 6cm,2.9cm
set key Left right bottom at screen .95,.1 spacing 1 maxrow 5 width -2 opaque
unset key
set xrange [-4.7:4.7]
set xlabel "curvature (\\num{E-3})"
set ylabel "moment (\\si{\\kilo\\newton\\meter})"
set xtics nomirror in -4,2,4
set yrange [-40:40]
set ytics nomirror in -30,15,30
set margins 0,0,0,0
set grid
set border 15
set output "PH1.tex"
plot "HINGE" every 2 using ($1)*1000:($2)*1000 w l lw 2 lc rgb "#a6cee3" title "plastic hinge 1"
set output
set output "PH2.tex"
plot "HINGE" every 2 using ($3)*1000:($4)*1000 w l lw 2 lc rgb "#1f78b4" title "plastic hinge 2"
set output
set output "PH3.tex"
plot "HINGE" every 2 using ($5)*1000:($6)*1000 w l lw 2 lc rgb "#b2df8a" title "plastic hinge 3"
set output
set output "PH4.tex"
plot "HINGE" every 2 using ($7)*1000:($8)*1000 w l lw 2 lc rgb "#33a02c" title "plastic hinge 4"
set output
set output "PH5.tex"
plot "HINGE" every 2 using ($9)*1000:($10)*1000 w l lw 2 lc rgb "#fb9a99" title "plastic hinge 5"
set output
set output "PH6.tex"
plot "HINGE" every 2 using ($11)*1000:($12)*1000 w l lw 2 lc rgb "#e31a1c" title "plastic hinge 6"
set output
set output "PH7.tex"
plot "HINGE" every 2 using ($13)*1000:($14)*1000 w l lw 2 lc rgb "#fdbf6f" title "plastic hinge 7"
set output
set output "PH8.tex"
plot "HINGE" every 2 using ($15)*1000:($16)*1000 w l lw 2 lc rgb "#ff7f00" title "plastic hinge 8"
set output
set output "PH9.tex"
plot "HINGE" every 2 using ($17)*1000:($18)*1000 w l lw 2 lc rgb "#cab2d6" title "plastic hinge 9"
set output
set output "PH10.tex"
plot "HINGE" every 2 using ($19)*1000:($20)*1000 w l lw 2 lc rgb "#6a3d9a" title "plastic hinge 10"
set output