set term tikz size 6cm,4cm
set output "TSCM.tex"
set xrange [0:4.2]
set yrange [0:1.2]
set format ""
#set xtics 1
#set ytics 0.5
set tics nomirror scale 0
set border 3
set margins 0,0,0,0
set arrow from graph 1,0 to graph 1.05,0 size screen 0.025,15,60 filled
set arrow from graph 0,1 to graph 0,1.05 size screen 0.025,15,60 filled
#set label "$f_{c,p}=\\SI{20}{\\mega\\pascal}$" at 3,1.05 left
#set arrow from 3.6,1 to 3,0.82 size screen 0.025,15,60 filled
#set label "$f_{c,p}=\\SI{60}{\\mega\\pascal}$" at 0.2,0.1 left
#set arrow from 0.9,0.15 to 1.6,0.28 size screen 0.025,15,60 filled
set xtics add("\\num{1}" 1)
set ytics add("$1$" 0.95)
set ytics add("$\\dfrac{\\sigma}{f_{c,p}}$" 1.2)
set xtics add("$\\varepsilon/\\varepsilon_{c,p}$" 4)
#set key samplen 2 spacing 1.3 right bottom at 0.0088,20
plot "TSCM" u 1:2 w l lw 2 lc rgb "#CC6600" notitle,\
     "TSCM" u 1:3 w l lw 2 lc rgb "#CC6600" notitle,\
     "TSCM" u 1:4 w l lw 2 lc rgb "#CC6600" notitle,\
     "TSCM" u 1:5 w l lw 2 lc rgb "#CC6600" notitle,\
     "TSCM" u 1:6 w l lw 2 lc rgb "#CC6600" notitle,\
     "TSCM" u 7:8 w l dt 2 lc rgb "#CC0066" notitle
set output
