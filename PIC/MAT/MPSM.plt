set term tikz size 6.5cm,4cm
set output "MPSM.tex"
set key samplen 1 width -1 spacing 2 right bottom at 18.5,-290
set xrange [-4:19]
set yrange [-550:550]
set xlabel "strain"
set ylabel "stress"
set mxtics 2
set mytics 2
set xtics format "" 
set ytics format ""
set margins 0,0,0,0
set label "$(\\varepsilon_0^0,\\sigma_0^0)$" at 2,400 offset -7,.6
set label "$(\\varepsilon_0^1,\\sigma_0^1)$" at 4,-376 offset 0,-.7
set label "$(\\varepsilon_0^2,\\sigma_0^2)$" at 2.77882039763126,403.115281590525 offset 0,-1
set label "$(\\varepsilon_0^3,\\sigma_0^3)$" at 10.0433821817324,-351.826471273071 offset 0,-1
set label "$(\\varepsilon_r^0,\\sigma_r^0)$" at 0,0 offset -6,1
set label "$(\\varepsilon_r^1,\\sigma_r^1)$" at 8,424 offset 0,-1
set label "$(\\varepsilon_r^2,\\sigma_r^2)$" at -1,-352.648797935727 offset 0,-1.2
set label "$(\\varepsilon_r^3,\\sigma_r^3)$" at 14,439.497092380459 offset 0,-1
set grid
plot 2e5*x/1E3 dt 2 lc rgb "#CC0066" notitle,\
     4e3*(x/1E3-.002)+400 dt 2 lc rgb "#CC0066" notitle,\
     4e3*(x/1E3+.002)-400 dt 2 lc rgb "#CC0066" notitle,\
     2e5*(x/1E3-.008)+424 dt 2 lc rgb "#CC0066" notitle,\
     2e5*(x/1E3+.001)-352.648797935727 dt 2 lc rgb "#CC0066" notitle,\
     2e5*(x/1E3-.014)+439.497092380459 dt 2 lc rgb "#CC0066" notitle,\
     "MPSM" using ($1*1E3):2 w l lc rgb "#0066CC" lw 3 notitle,\
     "POINTR" using ($1*1E3):2 pt 15 ps 2 t "$(\\varepsilon_r^n,\\sigma_r^n)$",\
     "POINT0" using ($1*1E3):2 pt 14 ps 2 t "$(\\varepsilon_0^n,\\sigma_0^n)$"
set output