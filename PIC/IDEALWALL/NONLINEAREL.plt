set term tikz size 14cm,4cm
set output "NONLINEAREL.tex"
set key center at 10,80 spacing 1 samplen 2 width 1 maxrow 4
set xrange [0:20]
set yrange [-110:140]
set xlabel "time (\\si{\\second})"
set ylabel "top displacement (\\si{\\milli\\metre})"
set xtics nomirror in 0,4,20
set ytics nomirror in -100,25,125
set margins 0,0,0,0
unset grid
set border 15
set multiplot
plot "NONLINEAREL" every 5 using 1:($2)*1000 w l lw 2 lc rgb "#e41a1c" title "GCMQ-G \\num{1x5}",\
     "NONLINEAREL" every 5 using 1:($3)*1000 w l lw 2 lc rgb "#377eb8" title "GCMQ-G \\num{2x10}",\
     "NONLINEAREL" every 5 using 1:($4)*1000 w l lw 2 lc rgb "#4daf4a" title "GCMQ-G \\num{4x20}"
set origin .675,.6
set size .3,.35
set xrange [0:30]
set yrange [*:*]
unset border
unset xlabel
unset ylabel
unset label
unset xtics
unset ytics
unset key
plot "ELNS" every 5 using 1:2 w l lw 1 lc rgb "#ff0000"
unset multiplot
set output