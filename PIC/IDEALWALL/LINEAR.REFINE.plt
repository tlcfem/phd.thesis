set term tikz size 6cm,4cm
set output "LINEAR.REFINE.tex"
set key center at 4,-100 spacing 1 samplen 2 width 1 maxrow 4
set xrange [0:7]
set yrange [-190:70]
set xlabel "time (\\si{\\second})"
set ylabel "top displacement (\\si{\\milli\\metre})"
set xtics nomirror in 0,2,6
set ytics nomirror in -180,60,60
set margins 0,0,0,0
unset grid
set border 15
plot "LINEAR" every 2 using 1:($2)*1000 w l lw 2 lc rgb "#e41a1c" title "GCMQ-I \\num{1x5}",\
     "LINEAR.REFINE" every 2 using 1:($2)*1000 w l lw 2 lc rgb "#377eb8" title "GCMQ-I \\num{4x20}",\
     "LINEAR" every 2 using 1:($5)*1000 w l lw 2 lc rgb "#984ea3" title "Q4 \\num{1x5}",\
     "LINEAR.REFINE" every 2 using 1:($3)*1000 w l lw 2 lc rgb "#ff7f00" title "Q4 \\num{4x20}"
set output