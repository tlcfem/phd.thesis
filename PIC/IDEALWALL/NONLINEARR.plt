set term tikz size 6cm,4cm
set output "NONLINEARR.tex"
set key center at 5,-160 spacing 1 samplen 2 width 1 maxrow 4
set xrange [0:7]
set yrange [-240:60]
set xlabel "time (\\si{\\second})"
set ylabel "top displacement (\\si{\\milli\\metre})"
set xtics nomirror in 0,2,6
set ytics nomirror in -240,60,60
set margins 0,0,0,0
unset grid
set border 15
plot "NONLINEAR1" every 2 using 1:($4)*1000 w l lw 2 lc rgb "#e41a1c" title "GCMQ-G \\num{1x5}",\
     "NONLINEAR2" every 2 using 1:($4)*1000 w l lw 2 lc rgb "#377eb8" title "GCMQ-G \\num{2x10}",\
     "NONLINEAR4" every 2 using 1:($4)*1000 w l lw 2 lc rgb "#4daf4a" title "GCMQ-G \\num{4x20}"
set output