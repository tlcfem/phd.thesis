set term tikz size 6cm,4cm
set output "NONLINEAR1.tex"
set key center at 5,-230 spacing 1 samplen 2 width 1 maxrow 4
set xrange [0:7]
set yrange [-330:80]
set xlabel "time (\\si{\\second})"
set ylabel "top displacement (\\si{\\milli\\metre})"
set xtics nomirror in 0,2,6
set ytics nomirror in -300,60,60
set margins 0,0,0,0
unset grid
set border 15
plot "NONLINEAR1" every 2 using 1:($2)*1000 w l lw 2 lc rgb "#e41a1c" title "GCMQ-I \\num{1x5}",\
     "NONLINEAR1" every 2 using 1:($3)*1000 w l lw 2 lc rgb "#377eb8" title "GCMQ-L \\num{1x5}",\
     "NONLINEAR1" every 2 using 1:($4)*1000 w l lw 2 lc rgb "#4daf4a" title "GCMQ-G \\num{1x5}",\
     "NONLINEAR1" every 2 using 1:($5)*1000 w l lw 2 lc rgb "#984ea3" title "Q4 \\num{1x5}"
set output