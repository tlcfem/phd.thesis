model basic -ndm 2 -ndf 3

set E 1
set V [expr 1./3.]
set AR 2
set L [expr 2.*$AR]
set NUM 16
set C .4

nDMaterial ElasticIsotropic 1 $E $V
nDMaterial PlaneStress 2 1

node 1 0 0
node 2 0 [expr $L/2.]
node 3 0 $L

if { $NUM == 2 } {
element SFI_MVLEM 1 1 2 $NUM $C -thick 1 1 -width 1 1 -mat 2 2
element SFI_MVLEM 2 2 3 $NUM $C -thick 1 1 -width 1 1 -mat 2 2
} elseif { $NUM == 4 } {
element SFI_MVLEM 1 1 2 $NUM $C -thick 1 1 1 1 -width .5 .5 .5 .5 -mat 2 2 2 2
element SFI_MVLEM 2 2 3 $NUM $C -thick 1 1 1 1 -width .5 .5 .5 .5 -mat 2 2 2 2
} elseif { $NUM == 8 } {
element SFI_MVLEM 1 1 2 $NUM $C -thick 1 1 1 1 1 1 1 1 -width .25 .25 .25 .25 .25 .25 .25 .25 -mat 2 2 2 2 2 2 2 2
element SFI_MVLEM 2 2 3 $NUM $C -thick 1 1 1 1 1 1 1 1 -width .25 .25 .25 .25 .25 .25 .25 .25 -mat 2 2 2 2 2 2 2 2
} elseif { $NUM == 16 } {
element SFI_MVLEM 1 1 2 $NUM $C -thick 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 -width .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 -mat 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
element SFI_MVLEM 2 2 3 $NUM $C -thick 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 -width .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 .125 -mat 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
} elseif { $NUM == 32 } {
element SFI_MVLEM 1 1 2 $NUM $C -thick 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 -width .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 -mat 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
element SFI_MVLEM 2 2 3 $NUM $C -thick 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 -width .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 .0625 -mat 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
}

fix 1 1 1 1

pattern Plain 1 Linear {
   load 3 1 0 0
}

system BandSPD
constraints Plain
integrator LoadControl 1
test NormDispIncr 1E-4 100
algorithm Newton
numberer RCM
analysis Static
analyze 1

print node 3