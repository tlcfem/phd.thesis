reset
set term tikz size 6cm,4cm
set output "ELE4.tex"
set key right bottom at screen .95,.1 samplen 1 spacing 1.1 width 1 maxrows 3
set xrange [.5:4]
set yrange [-30:20]
set xlabel "beam aspect ratio ($r$)"
set ylabel "tip deflection error (\\si{\\percent})"
set xtics .5
set ytics 10
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "ELE4" using 1:2 w linespoints lw 2 pt 1 lc rgb "#d7191c" title "$c=0.30$",\
     "ELE4" using 1:3 w linespoints lw 2 pt 2 lc rgb "#fdae61" title "$c=0.35$",\
     "ELE4" using 1:4 w linespoints lw 2 pt 3 lc rgb "#ffffbf" title "$c=0.40$",\
     "ELE4" using 1:5 w linespoints lw 2 pt 4 lc rgb "#abd9e9" title "$c=0.45$",\
     "ELE4" using 1:6 w linespoints lw 2 pt 5 lc rgb "#2c7bb6" title "$c=0.50$"
set output
set output "ROTA4.tex"
set yrange [-5:20]
set ytics 5
set ylabel "tip rotation error (\\si{\\percent})"
set key right top at screen .95,.95 samplen 1 spacing 1.1 width 1 maxrows 3
plot "ROTA4" using 1:2 w linespoints lw 2 pt 1 lc rgb "#d7191c" title "$c=0.30$",\
     "ROTA4" using 1:3 w linespoints lw 2 pt 2 lc rgb "#fdae61" title "$c=0.35$",\
     "ROTA4" using 1:4 w linespoints lw 2 pt 3 lc rgb "#ffffbf" title "$c=0.40$",\
     "ROTA4" using 1:5 w linespoints lw 2 pt 4 lc rgb "#abd9e9" title "$c=0.45$",\
     "ROTA4" using 1:6 w linespoints lw 2 pt 5 lc rgb "#2c7bb6" title "$c=0.50$"
set output