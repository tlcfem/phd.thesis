reset
set term tikz size 6cm,4cm
set output "AR2B.tex"
set key samplen 1 spacing 1.1 width 1 center at 4.5,12.5 maxrows 3
set xrange [2:6]
set yrange [-20:20]
set xlabel "number of chord elements"
set ylabel "tip deflection error (\\si{\\percent})"
set xtics 1
set ytics 10
set margins 0,0,0,0
set grid xtics ytics mxtics mytics lt 1 lw 1 lc rgb "#EEEEEE"
plot "AR2B" using 1:2 w linespoints lw 2 pt 1 lc rgb "#d7191c" title "$c=0.30$",\
     "AR2B" using 1:3 w linespoints lw 2 pt 2 lc rgb "#fdae61" title "$c=0.35$",\
     "AR2B" using 1:4 w linespoints lw 2 pt 3 lc rgb "#ffffbf" title "$c=0.40$",\
     "AR2B" using 1:5 w linespoints lw 2 pt 4 lc rgb "#abd9e9" title "$c=0.45$",\
     "AR2B" using 1:6 w linespoints lw 2 pt 5 lc rgb "#2c7bb6" title "$c=0.50$"
set output