set term tikz size 6.5cm,4cm
set output "LSW1.tex"
set key Left right bottom at screen .975,.075 samplen 1 width 1
set xrange [0:15]
set x2range [0:1.25]
set yrange [0:3]
set xlabel "top displacement (\\si{\\milli\\metre})"
set x2label "drift ratio (\\si{\\percent})"
set ylabel "base resistance (\\SI{E2}{\\kilo\\newton})"
set xtics nomirror in 0,3,12
set x2tics nomirror in 0,.25,1
set ytics nomirror in 0,1,2.5
set mytics 2
set margins 0,0,0,0
unset grid
set border 15
plot "LSW1-EXP" using 1:($2)/100 w l lw 2 lc rgb "#999999" title "LSW1",\
     "LSW1-2X2" using 1:($2)/100000 w lp pointinterval 11 lw 3 lc rgb "#e41a1c" title "SGCMQI",\
     "LSW1-2X2" using 1:($3)/100000 w lp pointinterval 17 lw 3 lc rgb "#377eb8" title "SGCMQL",\
     "LSW1-2X2" using 1:($4)/100000 w lp pointinterval 23 lw 3 lc rgb "#4daf4a" title "SGCMQG"
set output